# JP-FreeResourcesForAll
Making a website for free teaching resources, mostly the Japanese resources I've made over the years, to be free for people to use.

Hello! I'll be making a website for all of the Japanese language resources I've been making over the years.

I have little computing experience but I had fun with a HTML assessment in foundation year computing, making me want to do this as a side project that can hopefully be useful to others.

It will be basically be all past and future Japanese resources I've made. I may also use this as a future portfolio, and may add pages for hobby things and/or even computer programming things. It will be maintained more or less from here once it is live, at the moment it is in the creation step.
